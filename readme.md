# Dungeon Crawl Classics Character Sheets
A set of character sheets for the Dungeon Crawl Classics RPG.

### Contents
* `0level/` contains 0-level character sheets, 4 per page.
* `output/` contains pamphlet style character sheets for each class.
### Usage
`sh make.sh` to build the class sheets in a folder `output/`. This also builds a -noflip version which should be easier to read but won't print correctly.